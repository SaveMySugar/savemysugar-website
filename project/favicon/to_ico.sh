#!/bin/sh

set -e

XCF_FILE="favicon.xcf"

BASENAME="$(basename "$XCF_FILE" .xcf)"

PPM_FILE="${BASENAME}.ppm"
PPM_FILE_4BPP="${BASENAME}_4bpp.ppm"
ICO_FILE="${BASENAME}.ico"

convert "$XCF_FILE" "$PPM_FILE"

ppmquant 16 "$PPM_FILE" > "$PPM_FILE_4BPP"
rm "$PPM_FILE"

ppmtowinicon "$PPM_FILE_4BPP" > "$ICO_FILE" 
rm "$PPM_FILE_4BPP"
