DESTDIR := css

SOURCES	:= $(wildcard [^_]*.scss)
COMPILED := $(SOURCES:.scss=.css)

update: $(COMPILED)

clean:
	rm -rf $(DESTDIR) .sass-cache

%.css: %.scss
	[ -d $(DESTDIR) ] || mkdir -p $(DESTDIR)
	scss --compass --unix-newlines -t expanded --sourcemap=none $< $(DESTDIR)/$@
